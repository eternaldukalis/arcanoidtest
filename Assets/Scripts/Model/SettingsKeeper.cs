﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Model class for the game settings.
/// </summary>
public class SettingsKeeper : MonoBehaviour
{
    /// <summary>
    /// Current instance.
    /// </summary>
    public static SettingsKeeper Instance { private set; get; }

    /// <summary>
    /// Game settings.
    /// </summary>
    public Settings GameSettings
    {
        get
        {
            return gameSettings;
        }
    }

#pragma warning disable
    [SerializeField]
    Settings gameSettings;
#pragma warning restore

    private void Start()
    {
        if (Instance != null)
            Destroy(Instance);
        Instance = this;
    }
}

/// <summary>
/// Game settings.
/// </summary>
[System.Serializable]
public class Settings
{
    /// <summary>
    /// Base move speed for the stick.
    /// </summary>
    public float StickBaseMoveSpeed
    {
        get
        {
            return stickBaseMoveSpeed;
        }
    }
    /// <summary>
    /// Base move speed for the ball.
    /// </summary>
    public float BallBaseMoveSpeed
    {
        get
        {
            return ballBaseMoveSpeed;
        }
    }
    /// <summary>
    /// Ball's start direction.
    /// </summary>
    public Vector2 StartBallDirection
    {
        get
        {
            return startBallDirection;
        }
    }
    /// <summary>
    /// Ball's damage to blocks.
    /// </summary>
    public int BallDamage
    {
        get
        {
            return ballDamage;
        }
    }

#pragma warning disable
    [SerializeField]
    float stickBaseMoveSpeed;
    [SerializeField]
    float ballBaseMoveSpeed;
    [SerializeField]
    Vector2 startBallDirection;
    [SerializeField]
    int ballDamage;
#pragma warning restore
}
