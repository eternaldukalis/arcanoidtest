﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Model class for the game data.
/// </summary>
public class GameDataKeeper : MonoBehaviour {

    /// <summary>
    /// Current instance.
    /// </summary>
    public static GameDataKeeper Instance { private set; get; }

    /// <summary>
    /// Current game data.
    /// </summary>
    [SerializeField]
    public GameData Data
    {
        set
        {
            data = value;
        }
        get
        {
            return data;
        }
    }

    [SerializeField]
    GameData data;

	void Awake ()
    {
        if (Instance != null)
            Destroy(Instance);
        Instance = this;
	}
}

[System.Serializable]
public class GameData
{
    /// <summary>
    /// Lives remained.
    /// </summary>
    public int Lives
    {
        set
        {
            lives = value;
        }
        get
        {
            return lives;
        }
    }
    /// <summary>
    /// Current score.
    /// </summary>
    public int Score { set; get; }
    /// <summary>
    /// Whether player wins.
    /// </summary>
    public bool Win
    {
        set
        {
            win = value;
        }
        get
        {
            return win;
        }
    }

    [SerializeField]
    int lives;
    bool win = false;
}
