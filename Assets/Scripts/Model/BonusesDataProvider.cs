﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Determines bonus effect.
/// </summary>
public enum BonusType { BallSpeed, StickSpeed, DoubleScore }

/// <summary>
/// Model class for the bonuses.
/// </summary>
public class BonusesDataProvider : MonoBehaviour {

    /// <summary>
    /// Current instance.
    /// </summary>
    public static BonusesDataProvider Instance { private set; get; }

    /// <summary>
    /// Game bonuses.
    /// </summary>
    public Dictionary<string, BonusInfo> Bonuses
    {
        get
        {
            var res = new Dictionary<string, BonusInfo>();
            foreach (var x in bonuses)
            {
                if (res.ContainsKey(x.ID))
                {
                    Debug.LogWarning("Multiple bonuses with the same id.");
                    continue;
                }
                res.Add(x.ID, x);
            }
            return res;
        }
    }

#pragma warning disable
    [SerializeField]
    BonusInfo[] bonuses;
#pragma warning restore

    void Awake ()
    {
        if (Instance != null)
            Destroy(Instance);
        Instance = this;
	}
}

/// <summary>
/// Bonus info.
/// </summary>
[System.Serializable]
public class BonusInfo
{
    /// <summary>
    /// Bonus id.
    /// </summary>
    public string ID { get { return id; } }
    /// <summary>
    /// Bonus effect.
    /// </summary>
    public BonusType Type { get { return type; } }
    /// <summary>
    /// The time bonus longs.
    /// </summary>
    public float Time { get { return time; } }
    /// <summary>
    /// Effect's power.
    /// </summary>
    public float Value { get { return value; } }
    /// <summary>
    /// Bonus game object.
    /// </summary>
    public GameObject Prefab { get { return prefab; } }

#pragma warning disable
    [SerializeField]
    string id;
    [SerializeField]
    BonusType type;
    [SerializeField]
    float time;
    [SerializeField]
    float value;
    [SerializeField]
    GameObject prefab;
#pragma warning restore
}
