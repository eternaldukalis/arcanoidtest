﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Model class for the maps.
/// </summary>
public class MapsDataProvider : MonoBehaviour {

    /// <summary>
    /// Current instance.
    /// </summary>
    public static MapsDataProvider Instance { private set; get; }

    /// <summary>
    /// All maps info.
    /// </summary>
    public MapInfo[] Maps
    {
        get
        {
            return maps;
        }
    }

#pragma warning disable
    [SerializeField]
    MapInfo[] maps;
#pragma warning restore

    void Awake ()
    {
        if (Instance != null)
            Destroy(Instance);
        Instance = this;
	}
}

/// <summary>
/// Map info.
/// </summary>
[System.Serializable]
public class MapInfo
{
    /// <summary>
    /// Blocks' positions and ids.
    /// </summary>
    public PositionIDPair[] BlocksDisposition
    {
        get
        {
            return bloсksDisposition;
        }
    }
    /// <summary>
    /// Bonuses' positions and ids.
    /// </summary>
    public PositionIDPair[] BonusesDisposition
    {
        get
        {
            return bonusesDisposition;
        }
    }

#pragma warning disable
    [SerializeField]
    PositionIDPair[] bloсksDisposition;
    [SerializeField]
    PositionIDPair[] bonusesDisposition;
#pragma warning restore
}

/// <summary>
/// Pair of item's position and it's id.
/// </summary>
[System.Serializable]
public struct PositionIDPair
{
    /// <summary>
    /// Item's position.
    /// </summary>
    [SerializeField]
    public Vector2 Position;
    /// <summary>
    /// Item's id.
    /// </summary>
    [SerializeField]
    public string ID;
}