﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Model class for the blocks.
/// </summary>
public class BlocksDataProvider : MonoBehaviour {

    /// <summary>
    /// Current instance.
    /// </summary>
    public static BlocksDataProvider Instance { private set; get; }

    /// <summary>
    /// Blocks info.
    /// </summary>
    public Dictionary<string, BlockInfo> Blocks
    {
        get
        {
            var res = new Dictionary<string, BlockInfo>();
            foreach (var x in blocks)
            {
                if (res.ContainsKey(x.Id))
                {
                    Debug.LogWarning("Multiple blocks with the same id.");
                    continue;
                }
                res.Add(x.Id, x);
            }
            return res;
        }
    }

#pragma warning disable
    [SerializeField]
    BlockInfo[] blocks;
#pragma warning restore

    void Awake ()
    {
        if (Instance != null)
            Destroy(Instance);
        Instance = this;
	}
}

/// <summary>
/// Single block's info.
/// </summary>
[System.Serializable]
public class BlockInfo
{
    /// <summary>
    /// Block's id.
    /// </summary>
    public string Id
    {
        get
        {
            return id;
        }
    }
    /// <summary>
    /// Block's maximum health points.
    /// </summary>
    public int HealthPoints
    {
        get
        {
            return healthPoints;
        }
    }
    /// <summary>
    /// Score gained after block's destruction.
    /// </summary>
    public int Score
    {
        get
        {
            return score;
        }
    }
    /// <summary>
    /// Block's game object.
    /// </summary>
    public GameObject Prefab
    {
        get
        {
            return prefab;
        }
    }

#pragma warning disable
    [SerializeField]
    string id;
    [SerializeField]
    int healthPoints;
    [SerializeField]
    int score;
    [SerializeField]
    Sprite[] textures;
    [SerializeField]
    GameObject prefab;
#pragma warning restore

    /// <summary>
    /// Get block's texture based on its health points.
    /// </summary>
    /// <param name="hp">Block's health points.</param>
    /// <returns>Block's texture.</returns>
    public Sprite GetTexture(int hp)
    {
        if ((hp < 1) || (hp > HealthPoints))
        {
            Debug.LogError("Wrong health points value.");
            return null;
        }
        int size = HealthPoints / textures.Length;
        int index = (hp - 1) / size;
        return textures[index];
    }
}