﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Shows current score.
/// </summary>
public class UIScore : MonoBehaviour {

	void Start ()
    {
        GameDataController.OnScoreChanged += Set;
        Set();
	}

    private void OnDestroy()
    {
        GameDataController.OnScoreChanged -= Set;
    }

    private void Set()
    {
        GetComponent<Text>().text = GameDataController.Instance.Data.Score.ToString();
    }
}
