﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Shows result.
/// </summary>
public class UIResult : MonoBehaviour {

#pragma warning disable
    [SerializeField]
    GameObject parent, winMessage, loseMessage;
#pragma warning restore

    void Start ()
    {
        GameDataController.OnGameEnd += ShowResult;	
	}

    private void OnDestroy()
    {
        GameDataController.OnGameEnd -= ShowResult;
    }

    private void ShowResult()
    {
        parent.SetActive(true);
        if (GameDataController.Instance.Data.Win)
        {
            winMessage.SetActive(true);
            loseMessage.SetActive(false);
        }
        else
        {
            winMessage.SetActive(false);
            loseMessage.SetActive(true);
        }
    }
}
