﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILivesCount : MonoBehaviour {

	void Start ()
    {
        GameDataController.OnLivesChanged += Set;
        Set();
	}

    private void OnDestroy()
    {
        GameDataController.OnLivesChanged -= Set;
    }

    void Set()
    {
        GetComponent<Text>().text = GameDataController.Instance.Data.Lives.ToString();
    }
}
