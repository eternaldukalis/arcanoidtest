﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// In charge of block's appearence.
/// </summary>
public class BlockBehavior : MonoBehaviour {

    int num = 0;
    BlockInfo info;

	void Start ()
    {
        BlockCollision.OnBlockCollided += Change;
	}

    private void OnDestroy()
    {
        BlockCollision.OnBlockCollided -= Change;
    }

    private void Change(int arg1, int arg2)
    {
        if (arg1 != num)
            return;
        if (arg2 <= 0)
        {
            Destroy(this.gameObject);
            return;
        }
        SetTexture(info.GetTexture(arg2));
    }

    /// <summary>
    /// Set block's data.
    /// </summary>
    /// <param name="num">Block's number.</param>
    /// <param name="info">Block's info.</param>
    public void Set(int num, BlockInfo info)
    {
        this.num = num;
        this.info = info;
        SetTexture(info.GetTexture(info.HealthPoints));
    }

    void SetTexture(Sprite sprite)
    {
        var spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = sprite;
    }
}
