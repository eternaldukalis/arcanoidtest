﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Stick behavior.
/// </summary>
public class StickBehavior : MonoBehaviour {

    Vector2 speed;
    List<Direction> directions;
    Rigidbody2D rigidBody;

	void Start ()
    {
        directions = new List<Direction>();
        rigidBody = GetComponent<Rigidbody2D>();
        InputBase.OnStickMoveStart += StartMoving;
        InputBase.OnStickMoveStop += StopMoving;
	}

    private void OnDestroy()
    {
        InputBase.OnStickMoveStart -= StartMoving;
        InputBase.OnStickMoveStop -= StopMoving;
    }

    private void Update()
    {
        rigidBody.velocity = speed * BonusController.Instance.Multipliers[BonusType.StickSpeed];
    }

    void SetSpeed()
    {
        float proj = SettingsController.Instance.GameSettings.StickBaseMoveSpeed;
        float x = 0, y = 0;
        if (directions.Contains(Direction.Left))
            x -= proj;
        if (directions.Contains(Direction.Right))
            x += proj;
        if (directions.Contains(Direction.Down))
            y -= proj;
        if (directions.Contains(Direction.Up))
            y += proj;
        speed = new Vector2(x, y);
    }

    private void StopMoving(Direction obj)
    {
        if (directions.Contains(obj))
            directions.Remove(obj);
        SetSpeed();
    }

    private void StartMoving(Direction obj)
    {
        if (!directions.Contains(obj))
            directions.Add(obj);
        SetSpeed();
    }
}
