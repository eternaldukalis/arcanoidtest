﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controller class for the map.
/// </summary>
public class MapController : MonoBehaviour {

    /// <summary>
    /// Current instance.
    /// </summary>
    public static MapController Instance { private set; get; }

#pragma warning disable
    [SerializeField]
    GameObject blocksParent;
    int destroyedBlocks = 0;
    int maxBlocks;
#pragma warning restore

    void Awake ()
    {
        if (Instance != null)
            Destroy(Instance);
        Instance = this;

        BlockCollision.OnBlockDestroyed += HandleDestruction;
	}

    private void OnDestroy()
    {
        BlockCollision.OnBlockDestroyed -= HandleDestruction;
    }

    private void HandleDestruction(string obj)
    {
        int score = BlocksDataProvider.Instance.Blocks[obj].Score;
        GameDataController.Instance.AddScore((int)(score * BonusController.Instance.Multipliers[BonusType.DoubleScore]));
        destroyedBlocks++;
        if (destroyedBlocks >= maxBlocks)
        {
            GameDataController.Instance.SetWin();
        }
    }

    /// <summary>
    /// Build map.
    /// </summary>
    /// <param name="level">Level's number.</param>
    public void BuildMap(int level)
    {
        MapInfo info = MapsDataProvider.Instance.Maps[level];
        maxBlocks = info.BlocksDisposition.Length;
        for (var i = 0; i < info.BlocksDisposition.Length; i++)
        {
            string id = info.BlocksDisposition[i].ID;
            BlockInfo binfo = BlocksDataProvider.Instance.Blocks[id];
            GameObject obj = Instantiate(binfo.Prefab, blocksParent.transform);
            obj.transform.position = info.BlocksDisposition[i].Position;
            obj.GetComponent<BlockBehavior>().Set(i, binfo);
            obj.GetComponent<BlockCollision>().Set(i, binfo);
        }
        foreach (var x in info.BonusesDisposition)
        {
            BonusInfo binfo = BonusController.Instance.Bonuses[x.ID];
            GameObject obj = Instantiate(binfo.Prefab, blocksParent.transform);
            obj.transform.position = x.Position;
            obj.GetComponent<BonusCollision>().Set(x.ID);
        }
    }
}
