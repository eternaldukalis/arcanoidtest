﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles block's collisions.
/// </summary>
public class BlockCollision : MonoBehaviour {

    /// <summary>
    /// When block collides with ball.
    /// </summary>
    public static event System.Action<int, int> OnBlockCollided;
    /// <summary>
    /// When block is destroyed.
    /// </summary>
    public static event System.Action<string> OnBlockDestroyed;

    [SerializeField]
    string ballName = "ball";
    int num = 0;
    BlockInfo info;
    int hp = 0;

	void Start ()
    {
		
	}

    /// <summary>
    /// Set block's data.
    /// </summary>
    /// <param name="num">Block's number.</param>
    /// <param name="info">Block's info.</param>
    public void Set(int num, BlockInfo info)
    {
        this.num = num;
        this.info = info;
        hp = info.HealthPoints;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == ballName)
        {
            hp -= SettingsController.Instance.GameSettings.BallDamage;
            if (hp <= 0)
            {
                if (OnBlockDestroyed != null)
                    OnBlockDestroyed(info.Id);
            }
            if (OnBlockCollided != null)
                OnBlockCollided(num, hp);
        }
    }
}
