﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles collisions with bottom side.
/// </summary>
public class BottomCollision : MonoBehaviour {

    [SerializeField]
    string ballName = "";

	void Start ()
    {
		
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == ballName)
        {
            GameDataController.Instance.TakeLives(1);
        }
    }
}
