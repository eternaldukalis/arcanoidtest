﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles bonus' collisions.
/// </summary>
public class BonusCollision : MonoBehaviour {

    /// <summary>
    /// When bonus is activated.
    /// </summary>
    public static event System.Action<string> OnBonusActivated;

    [SerializeField]
    string ballName;
    string id;

	void Start ()
    {
		
	}

    /// <summary>
    /// Set bonus.
    /// </summary>
    /// <param name="id">Bonus' id.</param>
    public void Set(string id)
    {
        this.id = id;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == ballName)
        {
            if (OnBonusActivated != null)
                OnBonusActivated(id);
            Destroy(gameObject);
        }
    }
}
