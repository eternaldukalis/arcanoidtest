﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Input for desktop devices.
/// </summary>
public class DesktopInput : InputBase
{
#pragma warning disable
    [SerializeField]
    KeyCode[] RightButtons;
    [SerializeField]
    KeyCode[] LeftButtons;
    [SerializeField]
    KeyCode[] BallLaunchButtons;
#pragma warning restore

    private void Update()
    {
        foreach (var x in RightButtons)
            if (Input.GetKeyDown(x))
                StartStickMove(Direction.Right);
        foreach (var x in RightButtons)
            if (Input.GetKeyUp(x))
                StopStickMove(Direction.Right);

        foreach (var x in LeftButtons)
            if (Input.GetKeyDown(x))
                StartStickMove(Direction.Left);
        foreach (var x in LeftButtons)
            if (Input.GetKeyUp(x))
                StopStickMove(Direction.Left);

        foreach (var x in BallLaunchButtons)
            if (Input.GetKeyDown(x))
                LaunchBall();
        foreach (var x in BallLaunchButtons)
            if (Input.GetKeyUp(x))
                LaunchBall();
    }
}
