﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Moving direction.
/// </summary>
public enum Direction { Right, Left, Up, Down }

/// <summary>
/// Base input class.
/// </summary>
public abstract class InputBase : MonoBehaviour
{
    /// <summary>
    /// When stick starts moving.
    /// </summary>
    public static event System.Action<Direction> OnStickMoveStart;
    /// <summary>
    /// When stick stops moving.
    /// </summary>
    public static event System.Action<Direction> OnStickMoveStop;
    /// <summary>
    /// When ball is launched.
    /// </summary>
    public static event System.Action OnBallLaunch;

    [SerializeField]
    protected RuntimePlatform[] targetPlatforms;

    protected void Start()
    {
        bool isin = false;
        foreach (var x in targetPlatforms)
            if (x == Application.platform)
            {
                isin = true;
                break;
            }
        if (!isin)
            Destroy(this);
    }

    protected void StartStickMove(Direction direction)
    {
        if (OnStickMoveStart != null)
            OnStickMoveStart(direction);
    }

    protected void StopStickMove(Direction direction)
    {
        if (OnStickMoveStop != null)
            OnStickMoveStop(direction);
    }

    protected void LaunchBall()
    {
        if (OnBallLaunch != null)
            OnBallLaunch();
    }
}
