﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Ball's behavior.
/// </summary>
public class BallController : MonoBehaviour {

    Rigidbody2D rigidBody;
    float speed;
    bool isRunning = false;
    Vector3 startPosition;

	void Start ()
    {
        startPosition = transform.position;
        rigidBody = GetComponent<Rigidbody2D>();
        InputBase.OnBallLaunch += Launch;
        GameDataController.OnLivesChanged += ResetState;
        GameDataController.OnGameEnd += Remove;

	}

    private void OnDestroy()
    {
        InputBase.OnBallLaunch -= Launch;
        GameDataController.OnLivesChanged -= ResetState;
        GameDataController.OnGameEnd -= Remove;
    }

    private void Launch()
    {
        if (isRunning)
            return;
        isRunning = true;
        SetSpeed();
        Vector2 start = SettingsController.Instance.GameSettings.StartBallDirection.normalized * speed;
        rigidBody.velocity = start;
    }

    private void ResetState()
    {
        isRunning = false;
        speed = 0;
        rigidBody.velocity = new Vector2(0, 0);
        transform.position = startPosition;
    }

    void Remove()
    {
        Destroy(gameObject);
    }

    void SetSpeed()
    {
        speed = SettingsController.Instance.GameSettings.BallBaseMoveSpeed;
    }

    private void Update()
    {
        float realSpeed = speed * BonusController.Instance.Multipliers[BonusType.BallSpeed];
        if (rigidBody.velocity.sqrMagnitude != realSpeed * realSpeed)
            rigidBody.velocity = rigidBody.velocity.normalized * realSpeed;
    }
}
