﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Controls the game.
/// </summary>
public class GameController : MonoBehaviour {

    /// <summary>
    /// Current instance.
    /// </summary>
    public static GameController Instance { private set; get; }

	void Start ()
    {
        if (Instance != null)
            Destroy(Instance);
        Instance = this;

        // Test game start.
        StartGame(0);
	}

    /// <summary>
    /// Start the game.
    /// </summary>
    /// <param name="level">Game level.</param>
    public void StartGame(int level)
    {
        MapController.Instance.BuildMap(level);
    }

    /// <summary>
    /// Restart the level.
    /// </summary>
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
