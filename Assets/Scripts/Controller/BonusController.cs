﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controller class for bonuses.
/// </summary>
public class BonusController : MonoBehaviour {

    /// <summary>
    /// Current instance.
    /// </summary>
    public static BonusController Instance { private set; get; }

    /// <summary>
    /// Bonuses info.
    /// </summary>
    public Dictionary<string, BonusInfo> Bonuses { get { return BonusesDataProvider.Instance.Bonuses; } }
    
    /// <summary>
    /// Parameters multipliers.
    /// </summary>
    public Dictionary<BonusType, float> Multipliers { private set; get; }

	void Start ()
    {
        if (Instance != null)
            Destroy(Instance);
        Instance = this;

        Multipliers = new Dictionary<BonusType, float>();
        Multipliers.Add(BonusType.BallSpeed, 1);
        Multipliers.Add(BonusType.DoubleScore, 1);
        Multipliers.Add(BonusType.StickSpeed, 1);

        BonusCollision.OnBonusActivated += Activate;
	}

    private void OnDestroy()
    {
        BonusCollision.OnBonusActivated -= Activate;
    }

    private void Activate(string obj)
    {
        BonusInfo info = Bonuses[obj];
        StartCoroutine(bonus(info.Time, info.Type, info.Value));
    }

    IEnumerator bonus(float time, BonusType type, float value)
    {
        float tm = 0;
        Multipliers[type] *= value;
        while (tm < time)
        {
            tm += Time.deltaTime;
            yield return null;
        }
        Multipliers[type] /= value;
    }
}
