﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controller class for the game settings.
/// </summary>
public class SettingsController : MonoBehaviour {

    /// <summary>
    /// Current instance.
    /// </summary>
    public static SettingsController Instance { private set; get; }

    /// <summary>
    /// Game settings.
    /// </summary>
    public Settings GameSettings
    {
        get
        {
            return SettingsKeeper.Instance.GameSettings;
        }
    }

	void Start ()
    {
        if (Instance != null)
            Destroy(Instance);
        Instance = this;
	}
}
