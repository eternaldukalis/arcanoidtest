﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controller class for the game data.
/// </summary>
public class GameDataController : MonoBehaviour {

    /// <summary>
    /// Current instance.
    /// </summary>
    public static GameDataController Instance { private set; get; }

    /// <summary>
    /// When lives count is changed.
    /// </summary>
    public static event System.Action OnLivesChanged;
    /// <summary>
    /// When score is changed.
    /// </summary>
    public static event System.Action OnScoreChanged;
    /// <summary>
    /// When the game ends.
    /// </summary>
    public static event System.Action OnGameEnd;

    /// <summary>
    /// Current game data.
    /// </summary>
    public GameData Data
    {
        get
        {
            return GameDataKeeper.Instance.Data;
        }
    }

	void Awake ()
    {
        if (Instance != null)
            Destroy(Instance);
        Instance = this;
	}

    /// <summary>
    /// Take lives.
    /// </summary>
    /// <param name="count">Count of lives should be taken.</param>
    public void TakeLives(int count)
    {
        GameDataKeeper.Instance.Data.Lives -= count;
        if (OnLivesChanged != null)
            OnLivesChanged();
        if (Data.Lives <= 0)
        {
            if (OnGameEnd != null)
                OnGameEnd();
        }
    }

    /// <summary>
    /// Add score.
    /// </summary>
    /// <param name="amount">Score amount.</param>
    public void AddScore(int amount)
    {
        GameDataKeeper.Instance.Data.Score += amount;
        if (OnScoreChanged != null)
            OnScoreChanged();
    }

    /// <summary>
    /// Set win mode.
    /// </summary>
    public void SetWin()
    {
        GameDataKeeper.Instance.Data.Win = true;
        if (OnGameEnd != null)
            OnGameEnd();
    }
}
